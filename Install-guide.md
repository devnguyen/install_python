# Chuẩn bị cài đặt (Sử dụng với hệ điều hành Window)

Trên window để cài đặt và sử dụng Python có 3 cách:
1. Cài đặt bản Python "thuần" (Bản python download từ trang chủ)
2. Cài đặt Anaconda (Nếu cài phần 1 xong không gõ được python --version trên cmd)
3. Cài đặt Winpython (Không khuyến khích )


Dưới đây là các hướng dẫn với từng trường hợp:
## 1. Cài đặt bản Python "thuần"
Mặc định bản cài đặt Python này có rất ít các package. Sau này, khi dùng đến package nào thì mới install qua Pip. --> Lựa chọn tối ưu cho các lập trình viên chuyên nghiệp.

Vào link này để download bản Python 3.6.x mới nhất:
[https://www.python.org/downloads/windows/](https://www.python.org/downloads/windows/)

**Lưu ý:** chọn đùng phiên bản hệ điều hành hiện tại bạn đang sử dụng (Win32 (x86) /Win64) và chỉ chọn bản đã release (không download bản rc hoặc a1,a2,a3,..)

Ví dụ thời điểm tháng 12/2017 sẽ download bản `Python 3.6.4 - 2017-12-19` và không download bản `Python 3.6.4rc1 - 2017-12-05`, `Python 3.7.0a3 - 2017-12-05`


Install như các phần mềm thông thường.

**Lưu ý:** Tại các bước, sẽ có các ô checkbox hỏi việc cài đặt phần mềm, thực hiện check vào tất cả các checkbox được hỏi. Lưu ý nhớ vị trí cài đặt của phần mềm (PYTHON_PATH). Tham khảo bên dưới (tùy từng trường hợp)

Ví dụ:

![temp/01.png](temp/03.png)

![temp/01.png](temp/01.png)


![temp/01.png](temp/02.png)

Sau khi cài đặt xong, để đảm bảo phần mềm hoạt động tốt, hãy restart lại máy.

### Kiểm tra sau khi cài đặt
Kiểm tra xem PYTHON_PATH đã được Add path vào Environment chưa. 
Bước 1: Mở phần settings Environment theo hướng dẫn dưới đây:

[https://www.computerhope.com/issues/ch000549.htm](https://www.computerhope.com/issues/ch000549.htm)

Tại 2 phần User Variable và System Variable, thực hiện chọn biến môi trường PATH và chọn EDIT.

Kiểm tra xem trong biến PATH đã có đường dẫn đến thư mục cài đặt Python (PYTHON_PATH) hay chưa. (PYTHON_PATH được lưu từ bước cài đặt)

Thông thường có dạng
```
C:\Users\User_COM\AppData\Local\Programs\Python\Python36\
```

hoặc

```
C:\Users\User_COM\AppData\Local\Programs\Python\Python36-32
```

Lưu ý: AppData là thư mục ẩn --> cần bật hidden file

`User_COM` là tên account đang dùng trên máy

Nếu chưa có thực hiện add thêm đoạn PYTHON_PATH vào cuối (thêm dấu ; ở trước và thêm PYTHON_PATH vào sau)


Sau khi cài đặt xong, mở terminal/cmd và gõ:
```
python --version
```

Nếu hiển thị ra Python phiên bản bao nhiêu --> đã cài đặt thành công.

Nếu không hiển thị, thực hiện cài Anaconda

## 2. Cài đặt bản Anaconda.
Anaconda chứa phần lớn các thư viện "hịn" nhất của Python, người dùng gần như sẽ không phải cài thêm thư viện nữa
- Thích hợp cho việc học & làm việc ở môi trường offline (có policy chặn hết các loại kết nối internet)
- Thích hợp cho cả những "tay ngang" muốn học Python để tối ưu hóa công việc của mình (mà không cần phải suy nghĩ xem pip là cái gì).
- Anaconda hỗ trợ tối đa datasciense, rất nhiều video training từ bootcamp được anaconda cung cấp miễn phí.

Trong trường hợp download bản Python thuần và không sử dụng được như mong muốn (ví dụ như không setpath được) thì nên sử dụng anaconda

**Download**
[https://www.anaconda.com/download/](https://www.anaconda.com/download/)

Có đầy đủ các bản cho Window/Linux/MacOS. 
Bản nào cũng có cả hướng dẫn install luôn
- Linux: [https://docs.anaconda.com/anaconda/install/linux](https://docs.anaconda.com/anaconda/install/linux)
- Window: [https://docs.anaconda.com/anaconda/install/windows](https://docs.anaconda.com/anaconda/install/windows)
- MacOS: [https://docs.anaconda.com/anaconda/install/mac-os](https://docs.anaconda.com/anaconda/install/mac-os)

Việc cài đặt cũng như Python thuần, và chú ý phần PYTHON_PATH.

## 3.Cài đặt WinPython

Ngoài cách cài đặt thông thường, trên window có thể sử dụng bộ cài đặt [WinPython](http://winpython.github.io/). Đây là một "dạng môi trường ảo" của Python trên Window, nó là bản **portable**, dùng trực tiếp mà không phải lo đến việc cài đặt.

**Download**:
Lên trang blog của WinPython, chọn bản mới nhất để download.
[http://winpython.github.io/](http://winpython.github.io/)

Hoặc qua luôn trang Sourceforge của Winpython
[https://sourceforge.net/projects/winpython/files/](https://sourceforge.net/projects/winpython/files/)

![Install](temp/LuaChon1.png)

![Install](temp/LuaChon2.png)

![Install](temp/LuaChon3.png)

Mỗi version của WinPython có 2 bản: 
- WinPython-xxbit-3.a.b.cZero.exe: Chứa các thư viện cơ bản của Python nên dung lượng nhẹ (tầm < 30MB)
- WinPython-xxbit-3.a.b.cQT.exe: Chứa sẵn rất nhiều package như pip, ipython, jupyter, PyQt… Dung lượng nặng hơn (tầm 400MB)

Lưu ý:
- Download đúng phiên bản 32 hoặc 64 bit của máy tính đang sử dụng.
- Trong quá trình giải nén, cài đặt môi trường sẽ yêu cầu Microsoft Visual C++ Redistributable for Visual Studio 2015, có thể download tại [https://www.microsoft.com/en-us/download/details.aspx?id=48145](https://www.microsoft.com/en-us/download/details.aspx?id=48145)
- Nếu bạn vẫn còn dùng WinXP, hãy tải WinPython bản 3.4


**Giải nén**
Cách giải nén cũng đơn giản như cài đặt phần mềm:

![Install](temp/Install1.png)

![Install](temp/Install2.png)

Đợi chương trình chạy xong

**Sử dụng**
Vào thư mục vừa giải nén, sẽ có các phần mềm để bắt đầu làm việc với Python:

![Install](temp/Install3.png)



# Kiểm tra sau cài đặt:
Sau khi cài đặt xong, mở terminal/cmd và gõ:
```
python --version
```

Nếu hiển thị ra Python phiên bản bao nhiêu --> đã cài đặt thành công.

## Gõ dòng lệnh đầu tiên.

Sau khi cài đặt xong, tận hưởng thành quả của mình một chút nhỉ :)

Trên terminal/cmd, gõ:

```
python
```

Màn hình sẽ ra dấu nhắc >>> để chúng ta gõ lệnh, phía trên là các thông tin về phiên bản của python đã được cài đặt và chọn làm mặc định.

Tiếp tục gõ

```
print("Hello Python")
```

Màn hình ra kết quả: Hello Python

Vậy là bạn đã thành công với chương trình đầu tiên bằng Python.

Để thoát ra khỏi màn hình chờ nhập lệnh của Python, hãy gõ 

```
exit()
```

Note: 
Nếu màn hình không ra thì sao ? --> Hãy nhìn lên phiên bản, có lẽ bạn đang ở Python2.

Hãy thoát khỏi màn hình nhập lệnh và quay trở về màn hình terminal/cmd và gõ:

```
Python3
```
